import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
 
import org.junit.Test;

import java.util.Collection;

import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;

public class CompassDirectionsTest {

    Collection c;

    @Test
    public void testCardinals() {
        c = CompassDirection.cardinalDirections();
        assertTrue(c.contains(CompassDirection.N));
        assertTrue(c.contains(CompassDirection.E));
        assertTrue(c.contains(CompassDirection.S));
        assertTrue(c.contains(CompassDirection.W));
        assertFalse(c.contains(CompassDirection.NE));
        assertFalse(c.contains(CompassDirection.NW));
        assertFalse(c.contains(CompassDirection.SE));
        assertFalse(c.contains(CompassDirection.SW));
        assertEquals(c.size(), 4);
    }

    @Test
    public void testOrdinals() {
        c = CompassDirection.ordinalDirections();
        assertFalse(c.contains(CompassDirection.N));
        assertFalse(c.contains(CompassDirection.E));
        assertFalse(c.contains(CompassDirection.S));
        assertFalse(c.contains(CompassDirection.W));
        assertTrue(c.contains(CompassDirection.NE));
        assertTrue(c.contains(CompassDirection.NW));
        assertTrue(c.contains(CompassDirection.SE));
        assertTrue(c.contains(CompassDirection.SW));
        assertEquals(c.size(), 4);
    }
    @Test
    public void testPrincipals() {
        c = CompassDirection.principalDirections();
        assertTrue(c.contains(CompassDirection.N));
        assertTrue(c.contains(CompassDirection.E));
        assertTrue(c.contains(CompassDirection.S));
        assertTrue(c.contains(CompassDirection.W));
        assertTrue(c.contains(CompassDirection.NE));
        assertTrue(c.contains(CompassDirection.NW));
        assertTrue(c.contains(CompassDirection.SE));
        assertTrue(c.contains(CompassDirection.SW));
        assertEquals(c.size(), 8);
    }
}
