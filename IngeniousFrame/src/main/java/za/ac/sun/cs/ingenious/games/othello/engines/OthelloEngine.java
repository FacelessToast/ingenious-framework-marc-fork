package za.ac.sun.cs.ingenious.games.othello.engines;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;

import za.ac.sun.cs.ingenious.games.othello.gamestate.*;
import za.ac.sun.cs.ingenious.games.othello.network.*;

/**
 * The base engine class for the Othello (Reversi) board game. 
 * The engine represents a player and receives game updates and requests
 * from the Referee (server).
 */
public abstract class OthelloEngine extends Engine {

	protected OthelloBoard board;
	protected OthelloLogic logic = OthelloLogic.defaultOthelloLogic;
	protected OthelloFinalEvaluator evaluator = new OthelloFinalEvaluator();

	/** Whether debug printouts are used instead of the TUI */
	protected boolean debug = false;
	/** Whether output is enabled at all */
	protected boolean output = true;

	/**
	 * Constructs a new engine.
	 *
	 * @param serverConnection	the connection the engine uses to receive and send
	 *							information from and to the server
	 */
	public OthelloEngine(EngineToServerConnection serverConnection)
	{
		super(serverConnection);
	}
	
	/**
	 * Returns the Engine's name
	 *
	 * @return	the engine's name
	 */
	public abstract String engineName();

	/**
	 * Requests an action from the player and determines if the action is valid.
	 * If the action is valid, it is sent to the Referee. Otherwise the player
	 * is asked to generate a new move until the player's clock runs out.
	 *
	 * @param message	the request sent from the Referee
	 *
	 * @return			a valid action or null if the clock ran out
	 */
	public abstract PlayActionMessage receiveGenActionMessage(GenActionMessage message);
	
	/**
	 * Determines if the player wins or loses based on the final scores sent in
	 * the termination message.
	 *
	 * @param message	the game termination message sent from the Referee
	 */
	public void receiveGameTerminatedMessage(GameTerminatedMessage message)
	{
		if (output) {
			board.printPretty();
		}

		double scores[] = ((OthelloGameTerminatedMessage) message).getScores();
		Log.info("Player " + playerID + ":  game terminated received");

		if (scores[0] == GameFinalState.FAILURE.value 
				&& scores[1] == GameFinalState.FAILURE.value) {
			/* Something went wrong on the server's side */
			Log.error("OthelloEngine: Something bad happened server side.");
		} else if (scores[playerID] == GameFinalState.NOT_TERMINAL.value) {
			/* The player forfeit or lost by default */
			Log.info("You (player "+(playerID + 1)+") lost by default");
		} else if (scores[playerID ^ 1] == GameFinalState.NOT_TERMINAL.value) {
			/* The opponent forfeit or lost by default */
			Log.info("The opponent (player "+((playerID^1) + 1)+") lost by default");
		} else if (scores[playerID] > scores[playerID ^ 1]) {
			/* The player won */
			Log.info("You (player "+(playerID + 1)+") won!");
		} else if (scores[playerID] == scores[playerID ^ 1]) {
			/* It's a draw */
			Log.info("It's a draw!");
		} else {
			/* The player lost */
			Log.info("You (player "+(playerID + 1)+") lost!");
		}
	}
	
	/**
	 * Initialize the board.
	 *
	 * @param action	the initialization action sent from the Referee
	 */
	public void receiveInitGameMessage(InitGameMessage message)
	{
		this.playerID = ((OthelloInitGameMessage) message).getPlayerID();
		board = new OthelloBoard();
	}
	
	/**
	 * Executes a move on the current board sent from the Referee.
	 *
	 * @param message	the move message sent from the Referee
	 */
	public void receivePlayedMoveMessage(PlayedMoveMessage message)
	{
		Move move = message.getMove();
		logic.makeMove(board, move, false);
	}
}
