package za.ac.sun.cs.ingenious.games.go;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;

public class GoMctsFinalEvaluator extends GoFinalEvaluator implements MctsGameFinalEvaluator<TurnBasedSquareBoard> {
    private static final double WIN_VALUE  = 1;
    private static final double LOSE_VALUE = -1;
    private static final double DRAW_VALUE = 0;

    @Override
    public double[] getMctsScore(TurnBasedSquareBoard forState) {
        double[] scores = getScore(forState);
        if (scores[0] > scores[1]) {
            scores[0] = WIN_VALUE;
            scores[1] = LOSE_VALUE;
        } else if (scores[0] < scores[1]) {
            scores[0] = LOSE_VALUE;
            scores[1] = WIN_VALUE;
        } else {
            scores[0] = DRAW_VALUE;
            scores[1] = DRAW_VALUE;
        }
        return scores;
    }

    /**
     * Getter for the value of a win (For the MCTS result)
     *
     * @return 	The win value used in MCTS.
     */
    public double getWinValue() {
        return (double) WIN_VALUE;
    }

    /**
     * Getter for the value of a loss (For the MCTS result).
     *
     * @return 	The loss value used in MCTS.
     */
    public double getLossValue() {
        return (double) LOSE_VALUE;
    }

    /**
     * Getter for the value of a draw (For the MCTS result).
     *
     * @return 	The draw value used in MCTS.
     */
    public double getDrawValue() {
        return (double) DRAW_VALUE;
    }
}
