package za.ac.sun.cs.ingenious.games.go;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;

import java.io.IOException;

public abstract class GoEngine extends Engine {

    protected TurnBasedSquareBoard currentState;
    protected GoLogicThreadSafe logic;
    protected int TURN_LENGTH = 9000;
    protected ZobristHashing zobrist;
    protected long currentBoardHash;

    /**
     * @param toServer An established connection to the GameServer
     */
    public GoEngine(EngineToServerConnection toServer) throws IncorrectSettingTypeException, MissingSettingException, IOException {
        super(toServer);
        this.logic = new GoLogicThreadSafe();
        this.currentState = null;

        MatchSetting boardSettings = new MatchSetting("Go.json");
        int boardSize = boardSettings.getSettingAsInt("boardSize");
        zobrist = new ZobristHashing(boardSize);
        currentBoardHash = zobrist.hashBoard(new TurnBasedSquareBoard(boardSize, 0, 2));
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {
        zobrist = zobristHashing;
    }

    @Override
    public String engineName() {
        return "GoLegacyMCTSEngine";
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        GoFinalEvaluator eval = new GoFinalEvaluator();
        Log.info("Game has terminated");
        Log.info("Final scores:");
        double[] score = eval.getScore(currentState);
        for (int i = 0; i < score.length; i++) {
            Log.info("Player " + (i + 1) + ": " + score[i] + "");
        }
        Log.info("Final state:");
        currentState.printPretty();
        toServer.closeConnection();
        System.exit(0);
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        try {
            if (currentState == null) {
                this.currentState = new TurnBasedSquareBoard(((GoInitGameMessage) a).getBoardSize(), 0, 2);
            }
        } catch (Exception ex) {
            Log.info("GoLegacyMCTSEngine error:Problem creating board - " + ex.getLocalizedMessage());
        }
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        logic.makeMove(currentState, a.getMove());

        if (a.getMove() instanceof XYAction) {
//            XYAction action = (XYAction) a.getMove();
//            long xyActionIDtoXOR = zobrist.getPiecePlacementID(action.getX(), action.getY(), currentState.getBoardSize(), action.getPlayerID());
//            long hash = zobrist.XOR(currentBoardHash, xyActionIDtoXOR);
//            currentBoardHash = hash;

            long hash = zobrist.hashBoard(currentState);
            zobrist.addBoardToHashtable(hash, currentState);
        }
    }

    public void setCurrentBoardHash(long hash) {
        currentBoardHash = hash;
    }

}
