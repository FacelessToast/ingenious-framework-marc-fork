package za.ac.sun.cs.ingenious.games.ingenious.engines;


import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.games.ingenious.BagSequence;

public class Zobrist {
		int SEED = 17;
		Random rand = new Random(SEED);
		HashMap<Coord, ArrayList<Long>> zobrist = new HashMap<Coord,ArrayList<Long>>();
		int DIAMETER;

		/**
		 * zobrist contains every possible placement of X and O
		 * on an MxN board. The extra value denotes that the next
		 * player to play is O.
		 */
		public Zobrist(int BoardBreadth,int NumberOfColours,int rackSize,int numberOfPlayers){
			
			zobrist = new HashMap<Coord,ArrayList<Long>>();
			int SIDE_LENGTH = DIAMETER/2 +1;
			for (int i = -SIDE_LENGTH + 1; i < SIDE_LENGTH; i++) {
				for (int j = -SIDE_LENGTH + 1; j < SIDE_LENGTH; j++) {
					Coord coord = new Coord(i,j);
					zobrist.put(coord, new ArrayList<Long>());
					for(int k = 0; k< NumberOfColours;k++){
						zobrist.get(coord).add(rand.nextLong());
					}
				}
			}
			Coord rackIndex = new Coord(BoardBreadth,BoardBreadth);
			zobrist.put(rackIndex,new ArrayList<Long>());
			for(int i = 0; i< rackSize;i++){
				for(int j = 0; j < NumberOfColours;j++){
					for(int k = 0; k<NumberOfColours;k++){
						if(k>=j){
							zobrist.get(rackIndex.add(new Coord(j,k))).add(rand.nextLong());
						}
					}
				}
			}
						
			this.DIAMETER = BoardBreadth;
		}
		public Zobrist(int BoardBreadth,int NumberOfColours,int rackSize,int numberOfPlayers,BagSequence bag){
			
			zobrist = new HashMap<Coord,ArrayList<Long>>();
			int SIDE_LENGTH = DIAMETER/2 +1;
			for (int i = -SIDE_LENGTH + 1; i < SIDE_LENGTH; i++) {
				for (int j = -SIDE_LENGTH + 1; j < SIDE_LENGTH; j++) {
					Coord coord = new Coord(i,j);
					zobrist.put(coord, new ArrayList<Long>());
					for(int k = 0; k< NumberOfColours;k++){
						zobrist.get(coord).add(rand.nextLong());
					}
				}
			}
			Coord rackIndex = new Coord(BoardBreadth,0);
			zobrist.put(rackIndex,new ArrayList<Long>());
			for(int i = 0; i< rackSize;i++){
				zobrist.get(rackIndex.add(new Coord(0,-i))).add(rand.nextLong());
			}
			Coord bagIndex = new Coord(BoardBreadth+1,0);
			zobrist.put(bagIndex,new ArrayList<Long>());
			for(int i = 0; i< rackSize;i++){
				zobrist.get(bagIndex.add(new Coord(0,-i))).add(rand.nextLong());
			}
			
			this.DIAMETER = BoardBreadth;
		}
		
		public long getZobHash(int x, int y,int colours){
			return zobrist.get(new Coord(x,y)).get(colours);
		}
		
		public long getOToPlay(){
			return 0l;
		}
		
		
		public static void main(String [] args){
			Zobrist zob = new Zobrist(7,1,4,2);
			Log.info(zob.getZobHash(0, -2,0));
		}
		
	}
