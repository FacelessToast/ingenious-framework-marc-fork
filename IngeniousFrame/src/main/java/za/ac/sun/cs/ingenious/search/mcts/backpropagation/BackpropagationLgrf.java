package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR.LGRTable;

public class BackpropagationLgrf<N extends MctsNodeTreeParallelInterface<?, ?, ?>> implements BackpropagationThreadSafe<N> {

    public LGRTable visitedMovesTable = new LGRTable();

    public BackpropagationLgrf() {
    }

    /**
     * Update the fields relating to this backpropagation strategy for the current node.
     * @param node
     * @param results the win/loss or draw outcome for the simulation relating to the current playout.
     */
    public void propagate(N node, double[] results) {
        N parent = (N)node.getParent();
        double playerResults = results[parent.getPlayerID()];
        boolean win = false;
        if (playerResults == 1) {
            win = true;
        }

        if (win) {
            Action currentLgr;
            Action parentPreviousAction = parent.getPrevAction();
            if (parentPreviousAction != null) {
                currentLgr = visitedMovesTable.getMoveCombinationScores().get(parentPreviousAction);
                if (currentLgr == null) {
                    if (parentPreviousAction.getPlayerID() != -1 && node.getPrevAction().getPlayerID() != -1) {
                        visitedMovesTable.getMoveCombinationScores().put(parentPreviousAction, node.getPrevAction());
                    }
                } else {
                    visitedMovesTable.getMoveCombinationScores().replace(parentPreviousAction, node.getPrevAction());
                }
            }
        } else {
            Action currentLgr;
            Action parentPreviousAction = parent.getPrevAction();
            if (parentPreviousAction != null) {
                currentLgr = visitedMovesTable.getMoveCombinationScores().get(parentPreviousAction);
                if (currentLgr != null && currentLgr.equals(node.getPrevAction())) {
                    visitedMovesTable.getMoveCombinationScores().remove(parentPreviousAction);
                }
            }
        }
    }

    /**
     * @return the global table containing the move information relating to LGRF.
     */
    public LGRTable getLGRTable() {
        return visitedMovesTable;
    }

    public static <NN extends MctsNodeTreeParallelInterface<?, ?, ?>> BackpropagationLgrf<NN> newBackpropagationLgrf() {
        return new BackpropagationLgrf<>();
    }

}