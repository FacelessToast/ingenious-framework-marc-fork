package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.util.Comparator;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousMinMaxBoardInterface;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousRack;

public class MoveValueComparator implements Comparator<IngeniousAction> {

	private IngeniousScoreKeeper scores;
	private IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> board;
	public MoveValueComparator(IngeniousScoreKeeper score,IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> board){
		this.scores = score.copy();
		this.board = board;
	}
	public int compare(IngeniousAction o1, IngeniousAction o2) {
		int firstSum=0;
		int secondSum =0;
		
		// TODO commented out to avoid compile errors, sorry...
		//board.makeMove(o1);
		int [] firstScore = scores.calculateScore(board);
		//board.undoMove();
		

		//board.makeMove(o2);
		int [] secondScore = scores.calculateScore(board);
		//board.undoMove();
		
		for(int i : firstScore){
			firstSum+=i;
		}
		for(int i : secondScore){
			secondSum+=i;
		}
		if(firstSum>secondSum){
			return -1;
		}else if(firstSum==secondSum){
			return 0;
		}else{
			return 1;	
		}
	}

}
