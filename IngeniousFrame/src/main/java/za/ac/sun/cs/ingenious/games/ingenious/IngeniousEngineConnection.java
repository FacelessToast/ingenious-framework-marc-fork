package za.ac.sun.cs.ingenious.games.ingenious;

import com.esotericsoftware.minlog.Log;

import java.net.Socket;

import za.ac.sun.cs.ingenious.core.network.TCPProtocol;
import za.ac.sun.cs.ingenious.core.network.game.SocketWrapper;
import za.ac.sun.cs.ingenious.core.network.lobby.StringMessage;

public class IngeniousEngineConnection {

	private IngeniousController controller;
	private Socket client;
	private String playerName;
	private SocketWrapper messageHandler;
	public int id;

	public IngeniousEngineConnection(IngeniousController controller, Socket client) {
		this.controller = controller;
		this.client = client;

		//messageHandler = new MessageHandler(client);
		//messageHandler.openStreams();
		messageHandler.sendMessage(StringMessage.command(TCPProtocol.ID));
		try {

			String msg[] = ((StringMessage)messageHandler.receiveMessage()).asArray();
			this.id = Integer.parseInt(msg[1]);

			messageHandler.sendMessage(StringMessage.command(TCPProtocol.NAME));
			msg = ((StringMessage)messageHandler.receiveMessage()).asArray();

			this.playerName = msg[1];
			Log.info(playerName);

			controller.addEngine(this, this.id);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public String toString() {
		return playerName;
	}

	public void playMove(IngeniousAction move) {
//		messageHandler.command(TCPProtocol.PLAYMOVE, move.getTile().getTopColour() + "",
//				move.getTile().getBottomColour() + "", move.getTile()
//						.getRotation() + "", move.getPosition().getX() + "",
//				move.getPosition().getY() + "");
	}

	public void setRack(IngeniousRack rack, int playerId) {
//		Log.info("SET RACK CALLED");
//		String[] params = new String[rack.capacity() * 2 + 1];
//		params[0] = playerId + "";
//		int i = 1;
//		for (Tile tile : rack) {
//			params[i] = tile.getTopColour() + "";
//			params[i + 1] = tile.getBottomColour() + "";
//			i = i + 2;
//		}
//
//		messageHandler.command("setrack", params);
	}

	public void setBag(Tile[] bag) {
//		String[] params = new String[bag.length * 2];
//		
//		int i = 0;
//		for (Tile tile : bag) {
//			params[i] = tile.getTopColour() + "";
//			params[i + 1] = tile.getBottomColour() + "";
//			i = i + 2;
//		}
//		messageHandler.command("setbag", params);
	}

	public void draw(Tile tile, int player) {
//		messageHandler.command("draw", player + "", tile.getTopColour() + "",
//				tile.getBottomColour() + "");
	}

	public IngeniousAction genMove() {
//		messageHandler.command(TCPProtocol.GENMOVE);
//
//		String[] moveReply;
//
//		try {
//			moveReply = messageHandler.receiveMessage();
//			Log.info(moveReply.length + " LENGTH OF REPLY");
//		} catch (Exception e) {
//			return null;
//		}
//
//		if (moveReply[0].equals("=")) {
//			Tile tile = new Tile(Integer.parseInt(moveReply[1]),
//					Integer.parseInt(moveReply[2]));
//			tile.setRotation(Integer.parseInt(moveReply[3]),
//					controller.NUMBER_OF_COLOURS);
//			Coord coords = new Coord(Integer.parseInt(moveReply[4]),
//					Integer.parseInt(moveReply[5]));
//			return new IngeniousMove(tile, coords);
//		}
//
		return null;
	}
	

	
	// Found this in SocketWrapper.java, does not belong there...
//	/**
//	 * //TODO Ingenious stuff, needs to be abstracted to move
//	 * @param move
//	 */
//	public void reply(IngeniousMove move){
//		if(move == null){
//			errorReply("Null-move");
//		}
//		command("=",move.getTile().getTopColour() + "",
//				move.getTile().getBottomColour() + "", move
//				.getTile().getRotation() + "", move
//				.getPosition().getX() + "", move
//				.getPosition().getY() + "");
//	}
	
	
}
