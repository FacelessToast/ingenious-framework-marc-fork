package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

public class BackpropagationMast<N extends MctsNodeTreeParallelInterface<?, ?, ?>> implements BackpropagationThreadSafe<N> {

    public MastTable visitedMovesTable = null;

    public BackpropagationMast() {}

    /**
     * Update the fields relating to this backpropagation strategy for the current node.
     * @param node
     * @param results the win/loss or draw outcome for the simulation relating to the current playout.
     */
    public void propagate(N node, double[] results) {
        // Update visit count and win count
        N parent = (N)node.getParent();
        double playerResults = results[parent.getPlayerID()];
        boolean win = true;
        for (double i : results) {
            if (i > playerResults) {
                win = false;
                break;
            }
        }

        Action nodeAction = node.getPrevAction();
        MastTable.StoredAction previousAction = visitedMovesTable.getMoveCombinationScores().get(nodeAction);

        if (previousAction != null) {
            // Update the score of node action
            double oldValue = Math.exp((previousAction.getWins()/previousAction.getVisitCount())/visitedMovesTable.getTau());
            previousAction.incVisitCount();
            if (win) {
                previousAction.incWins();
            }
            double newValue = Math.exp((previousAction.getWins()/previousAction.getVisitCount())/visitedMovesTable.getTau());
            visitedMovesTable.decWinToVisitValues(oldValue);
            visitedMovesTable.incWinToVisitValues(newValue);
        } else {
            // Add new action to table
            MastTable.StoredAction newStoredAction = visitedMovesTable.createNewStoredAction(nodeAction);
            newStoredAction.incVisitCount();
            if (win) {
                newStoredAction.incWins();
            }
            visitedMovesTable.getMoveCombinationScores().put(newStoredAction.getAction(), newStoredAction);
            visitedMovesTable.incWinToVisitValues(Math.exp((newStoredAction.getWins()/newStoredAction.getVisitCount())/visitedMovesTable.getTau()));
        }
    }

    /**
     * @return the global table containing the move information relating to MAST.
     */
    public MastTable getQMastTable(double tau) {
        visitedMovesTable = new MastTable(tau);
        return visitedMovesTable;
    }

    public static <NN extends MctsNodeTreeParallelInterface<?, ?, ?>> BackpropagationMast<NN> newBackpropagationMast() {
        return new BackpropagationMast<>();
    }

}
