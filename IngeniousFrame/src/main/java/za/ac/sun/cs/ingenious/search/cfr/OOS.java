package za.ac.sun.cs.ingenious.search.cfr;

import com.esotericsoftware.minlog.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;

/**
 * 
 * This implements Online Outcome Sampling for any game. This implementation follows the
 * terminology and pseudocode given in "Online Monte Carlo Counterfactual Regret Minimization
 * for Search in Imperfect Information Games" by Viliam Lisý, Marc Lanctot and Michael Bowling.
 * 
 * @param <S> TurnBasedGameState this algorithm works with
 */
public class OOS<S extends TurnBasedGameState> {
	private Map<S, Map<Action, Double>> regretTables;
	private Map<S, Map<Action, Double>> strategyTables;
	private Map<S, Map<Action, Double>> profiles;

	private InformationSetDeterminizer<S> det;
	private GameLogic<S> logic;
	private GameFinalEvaluator<S> eval;
	private ActionSensor<S> sensor;
	private double e; // For sampling: balance between uniform and on-policy sampling at information sets of the active player

	public OOS (InformationSetDeterminizer<S> det, GameLogic<S> logic, GameFinalEvaluator<S> eval, ActionSensor<S> sensor, double e) {
		this.det = det;
		this.logic = logic;
		this.eval = eval;
		this.sensor = sensor;
		this.e = e;
		this.regretTables = new HashMap<S, Map<Action, Double>>();
		this.strategyTables = new HashMap<S, Map<Action, Double>>();
		this.profiles = new HashMap<S, Map<Action, Double>>();
	}
	
	/**
	 * Initializes the regretTable etc. for the given information set.
	 * The cumulative regret and cumulative average strategy profile are initialized to 0, while the current strategy
	 * at this information set is initialized to a uniform distribution over actions.
	 * 
	 * @param I Some information set
	 * @param actionsAtI List of actions available at I (could also be computed in this function, but is passed in as a minor optimization)
	 */
	private void initTable(S I, List<Action> actionsAtI) {
		Map<Action, Double> regretTable = new HashMap<Action, Double>();
		Map<Action, Double> strategyTable = new HashMap<Action, Double>();
		Map<Action, Double> profile = new HashMap<Action, Double>();
		double initialActionProb = 1.0/actionsAtI.size();
		for (Action a : actionsAtI) {
			regretTable.put(a, 0.0);
			strategyTable.put(a, 0.0);
			profile.put(a, initialActionProb);
		}
		regretTables.put(I, regretTable);
		strategyTables.put(I, strategyTable);
		profiles.put(I, profile);
	}
	
	/**
	 * Return value of the sample function
	 */
	private final class SampleReturnValue {
		final Action a; // Sampled action
		final double s1p; // Sample probability with information set targeting enabled
		final double s2p; // Sample probability without targeting
		public SampleReturnValue(Action a, double s1p, double s2p) {
			this.a = a;
			this.s1p = s1p;
			this.s2p = s2p;
		}
	}
	
	/**
	 * Get the probability of sampling a at information set I according to the distribution on page 5 of the above mentioned paper
	 */
	private double sampleProbForAction(S I, int i, Action a, double uniformProbAtI) {
		if (i == I.nextMovePlayerID) {
			return e * uniformProbAtI + (1.0-e) * profiles.get(I).get(a);
		} else {
			return profiles.get(I).get(a);
		}
	}

	// Sample an action for information set I according to the distribution on page 5 of the above mentioned paper
	private SampleReturnValue sample(S I, int i, double s1, double s2, List<Action> actionsAtI) {
		Action sampledAction = null;
		double sampledActionProbability = -1.0;
		if (profiles.containsKey(I)) {
			double uniformProbAtI = 1.0/actionsAtI.size();
			
			double sampleValue = Math.random();
			double cumulativeProbs = 0.0;
			for (Action a: actionsAtI) {
				double prob = sampleProbForAction(I, i, a, uniformProbAtI);
				cumulativeProbs += prob;
				if (sampledAction== null && cumulativeProbs > sampleValue) {
					sampledAction = a;
					sampledActionProbability = prob;
				}
			}
			if (!(1.0 - Helper.floatingTolerance < cumulativeProbs && cumulativeProbs < 1.0 + Helper.floatingTolerance)) {
				Log.error("OOS", "Cumulative probability is: " + cumulativeProbs);				
			}
		} else {
			sampledAction = actionsAtI.get((int) (Math.random() * actionsAtI.size()));		
			sampledActionProbability = 1.0/actionsAtI.size();
		}
		
		if (sampledAction == null) {
			Log.error("OOS", "Error sampling action from information set " + I + ". Is contained: " + profiles.containsKey(I));
			return null;
		}

		// TODO Add In-Match Search Targeting (so far, s1p is just set to s2p), see issue #236
		double s2p = sampledActionProbability * s2;
		double s1p = s2p;
		
		return new SampleReturnValue(sampledAction, s1p, s2p);
	}

	/**
	 * Return value of the oos function
	 */
	private final class OOSReturnValue {
		final double x; // suffix/tail reach probability for both players
		final double l; // root-to-leaf sample probability
		final double u; // payoff for update player
		public OOSReturnValue(double x, double l, double u) {
			this.x = x;
			this.l = l;
			this.u = u;
		}
	}

	/**
	 * The oos iteration, line numbers correspond to the pseudo code on page 4 of the above mentioned paper
	 */
	private OOSReturnValue oos(S h, double pii, double pinoti, double s1, double s2, int i) {
		// TODO Add In-Match Search Targeting (so far, no targeting is used, therefore delta is always 0), see issue #236
		double delta = 0.0;
		double oneMinusDelta = 1.0 - delta;
		// Lines 2-3
		if (logic.isTerminal(h)) {
			return new OOSReturnValue(1.0, delta * s1 + oneMinusDelta * s2, eval.getScore(h)[i]);
		}
		
		// TODO Add chance node handling, Lines 4-7, see issue #235
		
		// Line 8
		int playerActiveAtH = h.nextMovePlayerID;
		S I = det.observeState(h, playerActiveAtH);
		List<Action> actionsAtI = logic.generateActions(I, playerActiveAtH);
		// Line 9
		SampleReturnValue sampleRet = sample(I, i, s1, s2, actionsAtI);
		OOSReturnValue recCallResult = null;
		Map<Action, Double> profileAtI = null;
		Map<Action, Double> regretTableAtI = null;
		if (!profiles.containsKey(I)) {
			// Lines 11-12
			initTable(I, actionsAtI);
			profileAtI = profiles.get(I);
			regretTableAtI = regretTables.get(I);
			// Line 13
			logic.makeMove(h, sensor.fromPointOfView(sampleRet.a, h, -1));
			recCallResult = playout(h, (((delta*s1)+(oneMinusDelta*s2))*(1.0/actionsAtI.size())), i);
		} else {
			profileAtI = profiles.get(I);
			regretTableAtI = regretTables.get(I);
			// Line 15
			double sumCumulativeRegrets = 0.0;
			for (Action a : actionsAtI) {
				sumCumulativeRegrets += Math.max(regretTableAtI.get(a), 0.0);
			}		
			double uniformProb = 1.0/actionsAtI.size();
			for (Action a : actionsAtI) {
				if (sumCumulativeRegrets > 0) {
					profileAtI.put(a, Math.max(regretTableAtI.get(a), 0.0) / sumCumulativeRegrets);						
				} else {
					profileAtI.put(a, uniformProb);					
				}
			}
			// Line 16 & 17
			double pipph = profileAtI.get(sampleRet.a) * (i==playerActiveAtH?pii:pinoti);
			double pipnotph = (i==playerActiveAtH?pinoti:pii);
			// Line 18
			logic.makeMove(h, sensor.fromPointOfView(sampleRet.a, h, -1));
			recCallResult = oos(h, (i==playerActiveAtH?pipph:pipnotph), (i==playerActiveAtH?pipnotph:pipph), sampleRet.s1p, sampleRet.s2p, i);
		}
		
		// Line 19
		double c = recCallResult.x;
		// Line 20
		double x = recCallResult.x * profileAtI.get(sampleRet.a);
		
		// Lines 21-29
		for (Action ap : actionsAtI) {
			if (playerActiveAtH == i) {
				double W = recCallResult.u * pinoti / recCallResult.l;
				if (ap.equals(sampleRet.a)) {
					regretTableAtI.put(ap, regretTableAtI.get(ap) + (c-x)*W);
				} else {
					regretTableAtI.put(ap, regretTableAtI.get(ap) - x*W);
				}
			} else {
				Map<Action, Double> strategyTableAtI = strategyTables.get(I);
				strategyTableAtI.put(ap, strategyTableAtI.get(ap) + (1.0/(delta * s1 + oneMinusDelta * s2)) * pinoti * profileAtI.get(ap));
			}
		}
		
		// Line 30
		return new OOSReturnValue(x, recCallResult.l, recCallResult.u);
	}
	
	private OOS<S>.OOSReturnValue playout(S state, double s, int i) {
		double suffixProbability = 1.0;
		while(!logic.isTerminal(state)){
			// First generate all actions ...
			List<Action> actions = logic.generateActions(state, state.nextMovePlayerID);
			
			if (!actions.isEmpty()) { // ... if any actions are possible ...
				// ... apply a random one.
				Action randomAction = actions.get((int) (Math.random() * actions.size()));
				suffixProbability *= 1.0/actions.size();
				logic.makeMove(state, sensor.fromPointOfView(randomAction, state, -1));
			}
		}

		return new OOSReturnValue(suffixProbability, s * suffixProbability, eval.getScore(state)[i]);
	}

	/**
	 * Runs the OOS iteration the specified number of times.
	 * No return value, instead use getCumulativeStrategy and normalize using {@link Helper#getAverageStrategyProfile}
	 * 
	 * @param root State to run the iteration from.
	 * @param numIterations How many iterations to run
	 * @param wt Weight parameter
	 */
	public void iterate(S root, int numIterations, double wt) {
		long startTime = System.nanoTime();
		for (int t = 0; t < numIterations; t++) {
			for (int i = 0; i < 2; i++) {
				@SuppressWarnings("unchecked")
				S rootCopy = (S) root.deepCopy();
				oos(rootCopy, 1.0, 1.0, 1.0/wt, 1.0/wt, i);
			}
		}
		Log.info("OOS", numIterations + " iterations took " +  TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - startTime) + "s.");
	}

	/**
	 * @param I Some information set.
	 * @return Cumulative average strategy at this information set.
	 */
	public Map<Action, Double> getCumulativeStrategy(S I) {
		return strategyTables.get(I);
	}

}
