package za.ac.sun.cs.ingenious.core.util.hashing;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;

import java.util.Hashtable;
import java.util.Random;

/**
 * usage:
 //            long hash = zobristhash.hashBoard(node.getState());
 //            if (zobristhash.containsHash(hash)) {
 //            } else {
 //                zobristhash.addBoardToHashtable(hash, node.getState());
 //            }
 * @param <S>
 */
public class ZobristHashing<S extends TurnBasedSquareBoard> {

    /** The hashtable used to store generated Zobrist hashes. */
    public Hashtable<Long, S> hashes = null;

    /** Random  number generator */
    private static Random rand;

    private static int boardByteArrayLength = 0;
    long[][] pieceHashes;

    public ZobristHashing(int boardSideLength) {
        hashes = new Hashtable<>();
        rand = new Random();

        boardByteArrayLength = boardSideLength*boardSideLength;
        System.out.println(boardByteArrayLength);

        pieceHashes = new long[2][boardByteArrayLength];

        /** fills two 2D arrays with random values identifying a particular piece on a particular
         x,y coordinate on the board. */
        for(int i = 0; i < 2; i++) {
            for (int j = 0; j < boardByteArrayLength; j++) {
                pieceHashes[i][j] = rand.nextLong();
            }
        }
    }

    public void printHashes() {
        System.out.println("-----Printing hashes-------");
        for (long h: hashes.keySet()) {
            System.out.println(h);
        }
        System.out.println("---------------------------");
    }

    public long hashBoard(S state) {
        long hash = 0;
        byte[] board = state.board;
        for (int i = 0; i < boardByteArrayLength; i++) {
            if (board[i] == 1) {
                hash = XOR(hash, pieceHashes[0][i]);
            } else if (board[i] == 2) {
                hash = XOR(hash, pieceHashes[1][i]);
            }
        }

        return hash;
    }

    public void addBoardToHashtable(long hash, S state) {
        if (!hashes.containsKey(hash)) {
            hashes.put(hash, state);
        }
    }

    public long getPiecePlacementID(int x, int y, int width, int player) {
        return pieceHashes[player][y*width+x];
    }

    public long XOR(long hash, long piecePlacementID) {
        hash = hash ^ piecePlacementID;
        return hash;
    }

}
