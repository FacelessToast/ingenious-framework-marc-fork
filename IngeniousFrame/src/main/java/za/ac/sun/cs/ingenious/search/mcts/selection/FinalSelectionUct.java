package za.ac.sun.cs.ingenious.search.mcts.selection;

import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * A standard implementation of the TreeEngine selection strategy {@link TreeSelection},
 * that calculates UCT values to make a decision of which child node to select.
 * This strategy is specifically for TreeEngine selection and makes use of read locks
 * when viewing node information that is used in the calculations.
 *
 * @author Karen Laubscher
 *
 * @param <S>  The game state type.
 * @param <N>  The type of the mcts node.
 */
public class FinalSelectionUct<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, ?>> implements TreeSelectionFinal<N> {

	private GameLogic<S> logic;
	private final ReadWriteLock lock = new ReentrantReadWriteLock();

	/**
	 * Constructor to create a TreeEngine UCT selection object, since the constant C
	 * is not supplied, the default value for C is assigned as square root of 2.
	 *
	 * @param logic  The game logic
	 */
	private FinalSelectionUct(GameLogic<S> logic) {
		this.logic = logic;
	}

	/**
	 * The method that decide which child node to traverse to next, based on
	 * calculating the UCT value for each child and then selecting the child
	 * with the highest UCT value. Since the TreeEngine sturture is shared in TreeEngine
	 * parallelisation, nodes are (read) locked when information for
	 * the calculations are viewed.
	 *
	 * @param node  The current node whose children nodes are considered for the
	 *				next node to traverse to.
	 *
	 * @return      The selected child node with the highest UCT value.
	 */
	public N select(N node, ZobristHashing zobrist) {
		//Terminal state - no further selection takes place, therefore return node as selected node
		if (logic.isTerminal(node.getState())) {
			return node;
		}

		List<N> children;
		children = node.getChildren();
		N highestUctChild = null;
		double tempVal;
		node.getState().printPretty();
		double highestUct = Double.NEGATIVE_INFINITY;
		for (N child : children) {
			if (zobrist != null) {
				long hashForChildBoard = zobrist.hashBoard((TurnBasedSquareBoard) child.getState());
				if (!zobrist.hashes.containsKey(hashForChildBoard)) {
					if (child.getVisitCount() == 0) {
						tempVal = 0;
					} else {
						// choose robust child
						tempVal = child.getVisitCount();
					}
					if (Double.compare(tempVal, highestUct) > 0) {
						highestUctChild = child;
						highestUct = tempVal;
					}
				}
			} else {
				if (child.getVisitCount() == 0) {
					tempVal = 0;
				} else {
					// choose robust child
					tempVal = child.getVisitCount();
				}
				if (Double.compare(tempVal, highestUct) > 0) {
					highestUctChild = child;
					highestUct = tempVal;
				}
			}
		}
		if (highestUctChild == null && !children.isEmpty()) {
			highestUctChild = children.get(0);
		}
		return highestUctChild;
	}

	/**
	 * A static factory method, which creates a TreeEngine UCT selection object that
	 * uses the default value of square root 2 for the constant C in the UCTS
	 * calculation.
	 *
	 * This factory method is also a generic method, which uses Java type
	 * inferencing to encapsulate the complex generic type capturing required
	 * by the TreeEngine UCT selection constructor.
	 *
	 * @param logic   The game logic
	 */
	public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>> TreeSelectionFinal<NN> newFinalSelectionUct(GameLogic<SS> logic) {
		return new FinalSelectionUct<>(logic);
	}

	/**
	 * A static factory method, which creates a TreeEngine UCT selection object that
	 * uses the specified value for the constant C in the UCT value calculation.
	 *
	 * This factory method is also a generic method, which uses Java type
	 * inferencing to encapsulate the complex generic type capturing required
	 * by the TreeEngine UCT selection constructor.
	 *
	 * @param logic   The game logic
	 * @param c       The constant value for C (in the UCT calculation).
	 */
	public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>> TreeSelectionFinal<NN> newTreeSelectionUct(GameLogic<SS> logic, double c) {
		return new FinalSelectionUct<>(logic);
	}

}
