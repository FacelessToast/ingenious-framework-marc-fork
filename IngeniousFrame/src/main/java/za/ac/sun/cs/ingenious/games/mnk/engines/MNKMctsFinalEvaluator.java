package za.ac.sun.cs.ingenious.games.mnk.engines;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.util.state.TurnBased2DBoard;
import za.ac.sun.cs.ingenious.games.mnk.MNKFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
public class MNKMctsFinalEvaluator extends MNKFinalEvaluator implements MctsGameFinalEvaluator<MNKState> {

	private static final double WIN_VALUE  = 1;
	private static final double LOSE_VALUE = -1;
	private static final double DRAW_VALUE = 0;

	public int getWinner(MNKState state) {
		for (int i = 0; i < state.playerBoards.size(); i++) {
			TurnBased2DBoard board = state.playerBoards.get(i);
			// Rows
			for (int row = 0; row < board.getHeight(); row++) {
				int last = 0;
				int in_A_Row = 0;
				for (int col = 0; col < board.getWidth(); col++) {
					int curr = board.board[row][col];

					if (curr != 0 && curr == last) {
						in_A_Row++;
						if (in_A_Row == state.getK() - 1) {
							return curr - 1;
						}
					} else {
						in_A_Row = 0;
					}
					last = curr;
				}
			}

			// Collums
			for (int col = 0; col < board.getWidth(); col++) {
				int last = 0;
				int in_A_Row = 0;
				for (int row = 0; row < board.getHeight(); row++) {
					int curr = board.board[row][col];

					if (curr != 0 && curr == last) {
						in_A_Row++;
						if (in_A_Row == state.getK() - 1) {
							return curr - 1;
						}
					} else {
						in_A_Row = 0;
					}
					last = curr;
				}
			}

			// Diagonals (right)
			// For each field
			for (int row = 0; row < board.getHeight(); row++) {
				for (int col = 0; col < board.getWidth(); col++) {
					// Try to descend
					int last = 0;
					int in_A_Row = 0;
					for (int r = row, c = col; r < board.getHeight() && c < board.getWidth(); r++, c++) {
						int curr = board.board[r][c];
						if (curr != 0 && curr == last) {
							in_A_Row++;
							if (in_A_Row == state.getK() - 1) {
								return curr - 1;
							}
						} else {
							in_A_Row = 0;
						}
						last = curr;
					}
				}
			}

			// Diagonals (left)
			// For each field
			for (int row = 0; row < board.getHeight(); row++) {
				for (int col = 0; col < board.getWidth(); col++) {
					// Try to descend
					int last = 0;
					int in_A_Row = 0;
					for (int r = row, c = col; r < board.getHeight() && r >= 0 && c < board.getWidth()
							&& c >= 0; r++, c--) {
						int curr = board.board[r][c];
						if (curr != 0 && curr == last) {
							in_A_Row++;
							if (in_A_Row == state.getK() - 1) {
								return curr - 1;
							}
						} else {
							in_A_Row = 0;
						}
						last = curr;
					}
				}
			}

			if (full(board)) {
				return -1; // draw
			}
		}
		return -2; // not yet terminal
	}

	/**
	 * Getter for the value of a win (For the MCTS result)
	 *
	 * @return 	The win value used in MCTS.
	 */
	public double getWinValue() {
		return (double) WIN_VALUE;
	}

	/**
	 * Getter for the value of a draw (For the MCTS result)
	 *
	 * @return 	The draw value used in MCTS.
	 */
	public double getDrawValue() {
		return (double) DRAW_VALUE;
	}

	/**
	 * Getter for the value of a loss (For the MCTS result).
	 *
	 * @return 	The loss value used in MCTS.
	 */
	public double getLossValue() {
		return (double) LOSE_VALUE;
	}

	private boolean full(TurnBased2DBoard state) {
		for (int i = 0; i < state.getHeight(); i++) {
			for (int j = 0; j < state.getWidth(); j++) {
				if (state.board[i][j] == 0) {
					return false;
				}
			}
		}
		return true;
	}

	public double[] getMctsScore(MNKState forState) {
		double[] scores = new double[2];
		scores[0] = LOSE_VALUE;
		scores[1] = LOSE_VALUE;
		if(getWinner(forState)==-1){
			scores[0] = DRAW_VALUE;
			scores[1] = DRAW_VALUE;
		}else{
			scores[getWinner(forState)] = WIN_VALUE;
		}
		return scores;
	}
}
