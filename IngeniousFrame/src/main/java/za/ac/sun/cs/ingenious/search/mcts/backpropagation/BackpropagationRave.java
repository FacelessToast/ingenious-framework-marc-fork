package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE.MctsRaveNodeExtensionParallel;

import java.util.ArrayList;
import java.util.Hashtable;

public class BackpropagationRave<N extends MctsNodeTreeParallelInterface<?, ?, ?>> implements BackpropagationThreadSafe<N> {

    private ThreadLocal<ArrayList<Action>> movesOnPath = ThreadLocal.withInitial(ArrayList::new);

    public BackpropagationRave() {
    }

    /**
     * Update the fields relating to this backpropagation strategy for the current node.
     * @param node
     * @param results the win/loss or draw outcome for the simulation relating to the current playout.
     */
    public void propagate(N node, double[] results) {
        N parentNode = (N) node.getParent();
        Action previousAction = node.getPrevAction();

        double playerResults = results[parentNode.getPlayerID()];

        Hashtable<String, MctsNodeExtensionParallelInterface> enhancementsParentNode = parentNode.getEnhancementClasses();
        node.readLockEnhancementClassesArrayList();
        MctsRaveNodeExtensionParallel raveEnhancementExtensionParentNode = (MctsRaveNodeExtensionParallel)enhancementsParentNode.get("Rave");
        node.readUnlockEnhancementClassesArrayList();
        if (raveEnhancementExtensionParentNode == null) {
            System.out.println("No rave enhancement to backprop from in Rave backpropagation class");
        }

        // Update Monte-Carlo values
        boolean win = true;
        for (double i : results) {
            if (i > playerResults) {
                win = false;
                break;
            }
        }

        // Add the move last played to the thread specific global arraylist of moves played on this backprop path
        if (!movesOnPath.get().contains(previousAction)) {
            movesOnPath.get().add(previousAction);
        }

        // Update the RAVE values for this node's parent
        if (!movesOnPath.get().isEmpty()) {
            raveEnhancementExtensionParentNode.updateRaveValues(win, movesOnPath.get());
        }
    }

    /**
     * Update the thread local list of moves made during this playout with the moves made during the
     * simulation phase of this playout.
     * @param movesFromSim the moves made during the simulation phase of this playout.
     */
    public void updateMovesFromSimulation(ArrayList<Action> movesFromSim) {
        movesOnPath.get().clear();
        for (int i = 0 ;i < movesFromSim.size(); i++) {
            Action action = movesFromSim.get(i);
            if (!movesOnPath.get().contains(action)) {
                movesOnPath.get().add(action);
            }
        }
    }

    public static <NN extends MctsNodeTreeParallelInterface<?, ?, ?>> BackpropagationRave<NN> newBackpropagationRave() {
        return new BackpropagationRave<>();
    }

}