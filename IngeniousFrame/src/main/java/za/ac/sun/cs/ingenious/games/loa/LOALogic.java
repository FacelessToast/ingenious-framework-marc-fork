package za.ac.sun.cs.ingenious.games.loa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;

public class LOALogic implements TurnBasedGameLogic<LOABoard> {

	@Override
	public boolean validMove(LOABoard fromState, Move rawMove) {
		LOAAction move = (LOAAction) rawMove;
		int rF = move.getFrom().getY();
		int cF = move.getFrom().getX();
		int rT = move.getTo().getY();
		int cT = move.getTo().getX();

		// No piece to move or trying to move opponent's piece
		if ((fromState.getState()[rF][cF] == LOABoard.EMPTY) || (fromState.getState()[rF][cF] != fromState.nextMovePlayerID)){
			return false;
		}

		// Landing on own piece
		if (fromState.getState()[rF][cF] == fromState.getState()[rT][cT]) {
			return false;
		}

		// Check that player moves the correct number of spaces and doesn't jump over
		// enemy's pieces
		if (rF == rT){
			// Horizontal
			int numPieces = 0;
			for (int i = 0; i < fromState.getBoardSize(); i++){
				if (fromState.getState()[rF][i] != LOABoard.EMPTY){
					numPieces++;
					if (cT > cF){
						if ((i > cF) && (i < cT) && (fromState.getState()[rF][i] !=fromState.nextMovePlayerID)){
							return false;
						}
					} else if (cF > cT) {
						if ((i > cT) && (i < cF) && (fromState.getState()[rF][i] !=fromState.nextMovePlayerID)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(cT - cF)){
				return false;
			}
		} else if (cF == cT){
			// Vertical
			int numPieces = 0;
			for (int i = 0; i < fromState.getBoardSize(); i++){
				if (fromState.getState()[i][cF] != LOABoard.EMPTY){
					numPieces++;
					if (rT > rF){
						if ((i > rF) && (i < rT) && (fromState.getState()[i][cF] !=fromState.nextMovePlayerID)){
							return false;
						}
					} else if (cF > cT) {
						if ((i > rT) && (i < rF) && (fromState.getState()[i][cF] !=fromState.nextMovePlayerID)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(rT - rF)){
				return false;
			}
		} else {
			if ((double)(rF - rT) / (double)(cF - cT) == 1){
				// Positive diagonal
				int numPieces = 0;
				for (int i = rF, j = cF; i < fromState.getBoardSize() && j < fromState.getBoardSize(); i++, j++){
					if (fromState.getState()[i][j] != LOABoard.EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (fromState.getState()[i][j] !=fromState.nextMovePlayerID)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (fromState.getState()[i][j] !=fromState.nextMovePlayerID)){
								return false;
							}
						}
					}
				}
				for (int i = rF - 1, j = cF - 1; i >= 0 && j >= 0; i--, j--){
					if (fromState.getState()[i][j] != LOABoard.EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (fromState.getState()[i][j] !=fromState.nextMovePlayerID)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (fromState.getState()[i][j] !=fromState.nextMovePlayerID)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(rT - rF)){
					return false;
				}
			} else if ((double)(rF - rT) / (double)(cF - cT) == -1) {
				// Negative diagonal
				int numPieces = 0;
				for (int i = rF, j = cF; i >= 0 && j < fromState.getBoardSize(); i--, j++){
					if (fromState.getState()[i][j] != LOABoard.EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (fromState.getState()[i][j] !=fromState.nextMovePlayerID)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (fromState.getState()[i][j] !=fromState.nextMovePlayerID)){
								return false;
							}
						}
					}
				}
				for (int i = rF + 1, j = cF - 1; i < fromState.getBoardSize() && j >= 0; i++, j--){
					if (fromState.getState()[i][j] != LOABoard.EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (fromState.getState()[i][j] !=fromState.nextMovePlayerID)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (fromState.getState()[i][j] !=fromState.nextMovePlayerID)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(rF - rT)){
					return false;
				}
			} else {
				// Not a straight line
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean makeMove(LOABoard fromState, Move rawMove) {
		LOAAction move = (LOAAction)rawMove;
		
		int rF = move.getFrom().getY();
		int cF = move.getFrom().getX();
		int rT = move.getTo().getY();
		int cT = move.getTo().getX();
		if (validMove(fromState, move)){
			fromState.previousFrom.push(new Coord(cF, rF));
			fromState.previousTo.push(new Coord(cT, rT));
			if ((fromState.getState()[rT][cT] != LOABoard.EMPTY) && (fromState.getState()[rT][cT] != fromState.nextMovePlayerID)){
				fromState.capture.push(true);
				if (fromState.nextMovePlayerID == LOABoard.WHITE){
					fromState.numBlack--;
				} else {
					fromState.numWhite--;
				}
			} else {
				fromState.capture.push(false);
			}
			fromState.getState()[rT][cT] = fromState.nextMovePlayerID;
			fromState.getState()[rF][cF] = LOABoard.EMPTY;
			if (fromState.nextMovePlayerID == LOABoard.BLACK){
				fromState.nextMovePlayerID = LOABoard.WHITE;
			} else {
				fromState.nextMovePlayerID = LOABoard.BLACK;
			}
			return true;
		}
		return false;
	}

	@Override
	public void undoMove(LOABoard fromState, Move move) {
		Coord from = fromState.previousFrom.pop();
		Coord to = fromState.previousTo.pop();
		boolean cap = fromState.capture.pop();
		fromState.getState()[from.getY()][from.getX()] = fromState.getState()[to.getY()][to.getX()];
		if (cap){
			fromState.getState()[to.getY()][to.getX()] = fromState.nextMovePlayerID;
			if (fromState.nextMovePlayerID == LOABoard.WHITE){
				fromState.numWhite++;
			} else {
				fromState.numBlack++;
			}
		} else {
			fromState.getState()[to.getY()][to.getX()] = LOABoard.EMPTY;
		}
		if (fromState.nextMovePlayerID == LOABoard.BLACK){
			fromState.nextMovePlayerID = LOABoard.WHITE;
		} else {
			fromState.nextMovePlayerID = LOABoard.BLACK;
		}
	}

	@Override
	public List<Action> generateActions(LOABoard fromState, int forPlayerID) {
		//curPlayer is never set, use this.nextMovePlayerID instead.
		ArrayList<Coord> loc = new ArrayList<Coord>();
		for (int i = 0; i < fromState.getBoardSize(); i++){
			for (int j = 0; j < fromState.getBoardSize(); j++){
				if (fromState.getState()[i][j] == fromState.nextMovePlayerID){
					loc.add(new Coord(j, i));
				}
			}
		}
		
		Random rand = new Random();
		int numElements = loc.size();
		ArrayList<Action> checked = new ArrayList<Action>();
		for (int k = 0; k < numElements; k++){
			Coord from = loc.remove(rand.nextInt(loc.size()));
			for (int i = 0; i < fromState.getBoardSize(); i++){
				for (int j = 0; j < fromState.getBoardSize(); j++){
					LOAAction m = new LOAAction(from, new Coord(j, i));
					if (validMove(fromState, m)){
						checked.add(m);
						return checked;
					}
				}
			}	
		}
		return null;
	}

	public List<Action> generateActions(LOABoard fromState, int forPlayerID, Action action) {
		//curPlayer is never set, use this.nextMovePlayerID instead.
		ArrayList<Coord> loc = new ArrayList<Coord>();
		for (int i = 0; i < fromState.getBoardSize(); i++){
			for (int j = 0; j < fromState.getBoardSize(); j++){
				if (fromState.getState()[i][j] == fromState.nextMovePlayerID){
					loc.add(new Coord(j, i));
				}
			}
		}

		Random rand = new Random();
		int numElements = loc.size();
		ArrayList<Action> checked = new ArrayList<Action>();
		for (int k = 0; k < numElements; k++){
			Coord from = loc.remove(rand.nextInt(loc.size()));
			for (int i = 0; i < fromState.getBoardSize(); i++){
				for (int j = 0; j < fromState.getBoardSize(); j++){
					LOAAction m = new LOAAction(from, new Coord(j, i));
					if (validMove(fromState, m)){
						checked.add(m);
						return checked;
					}
				}
			}
		}
		return null;
	}

	@Override
	public boolean isTerminal(LOABoard state) {
		return (whiteWins(state) || blackWins(state));
	}

	private boolean whiteWins(LOABoard board){
		int[][] state = board.getState();
		boolean win = false;
		outer: {
			for (int i = 0; i < state.length; i++){
				for (int j = 0; j < state.length; j++){
					if (state[i][j] == LOABoard.WHITE){
						win = connected(board, new Coord(i, j));
						break outer;
					}
					
				}
			}
		}	
		return win;
	}
	
	private boolean blackWins(LOABoard board){
		int[][] state = board.getState();
		boolean win = false;
		outer: {
			for (int i = 0; i < state.length; i++){
				for (int j = 0; j < state.length; j++){
					if (state[i][j] == LOABoard.BLACK){
						win = connected(board, new Coord(i, j));
						break outer;
					}
					
				}
			}
		}	
		return win;
	}
	
	private boolean connected(LOABoard board,Coord c){
		int[][] state = board.getState();
		int colour = state[c.getY()][c.getX()];
		LinkedList<Coord> q = new LinkedList<Coord>();
		HashSet<Coord> checked = new HashSet<Coord>();
		q.add(c);
		while(!q.isEmpty()){
			Coord current = q.remove();
			int row = current.getY();
			int col = current.getX();
			if (state[row][col] == colour){
				checked.add(current);
				Coord n = new Coord(row - 1, col);
				Coord s = new Coord(row + 1, col);
				Coord e = new Coord(row, col + 1);
				Coord w = new Coord(row, col - 1);
				Coord ne = new Coord(row - 1, col + 1);
				Coord nw = new Coord(row - 1, col - 1);
				Coord se = new Coord(row + 1, col + 1);
				Coord sw = new Coord(row + 1, col - 1);
				if ((row > 0) && !checked.contains(n)){
					q.add(n);
				}
				if ((row < state.length - 1) && !checked.contains(s)){
					q.add(s);	
				}
				if ((col < state.length - 1) && !checked.contains(e)){
					q.add(e);
				}
				if ((col > 0) && !checked.contains(w)){
					q.add(w);
				}
				if ((row > 0) && (col < state.length - 1) && !checked.contains(ne)){
					q.add(ne);
				}
				if ((row > 0) && (col > 0) && !checked.contains(nw)){
					q.add(nw);
				}
				if ((col < state.length - 1) && (row < state.length - 1) && !checked.contains(se)){
					q.add(se);
				}
				if ((col > 0) && (row < state.length - 1) && !checked.contains(sw)){
					q.add(sw);
				}
			}
		}
		if (colour == LOABoard.WHITE){
			return checked.size() == board.numWhite;
		} else {
			return checked.size() == board.numBlack;
		}
	}
}
