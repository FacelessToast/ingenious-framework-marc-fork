package za.ac.sun.cs.ingenious.core.util.referee;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.Referee;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;

/**
 * A superclass that should work for most referees. The run method sends customizable
 * {@link InitGameMessage}s and {@link GameTerminatedMessage}s at the start and end of a
 * game. Furthermore, at the end of the game the results are logged and displayed on
 * screen.
 */
public abstract class GeneralReferee<S extends GameState, L extends GameLogic<S>, E extends GameFinalEvaluator<S>>
		extends Referee {

	protected S currentState;
	protected L logic;
	protected E eval;
	protected ActionSensor<S> sensor;
	protected boolean terminated;

	public GeneralReferee(MatchSetting match, PlayerRepresentation[] players, S currentState, L logic, E eval, ActionSensor<S> sensor) {
		super(match, players);
		this.currentState = currentState;
		this.logic = logic;
		this.eval = eval;
		this.sensor = sensor;
		this.terminated = false;
	}
	
	public S getCurrentState() {
		return currentState;
	}

	/**
	 * Overwrite this method if your game requieres specific initialisation.
	 * @return The init game message that is to be sent to every player before the game starts.
	 */
	protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
		return new InitGameMessage();
	}

	/**
	 * Overwrite this method if your game requieres specific termination.
	 * @return The game terminate message that is to be sent to every player after the game ended.
	 */
	protected GameTerminatedMessage createTerminateGameMessage(PlayerRepresentation player) {
		return new GameTerminatedMessage();
	}

	/**
	 * Overwrite this method if your game requieres a specific GenActionMessage, e.g. supplying tiles, updating the deck, etc.
	 * @return The genMove message that is to be sent to the current player.
	 */
	protected GenActionMessage createGenActionMessage(PlayerRepresentation player) {
		return new GenActionMessage();
	}

	/**
	 * Overwrite this method if you want to do custom things before the game starts. 
	 */
	protected void beforeGameStarts() {
		
	}

	/**
	 * Overwrite this method if you want to do custom things after the game terminated. 
	 */
	protected void afterGameTerminated() {
		Log.info("Game has terminated!");
        Log.info("Final scores:");
		double[] score = eval.getScore(currentState);
        for (int i = 0; i < score.length; i++) {
            Log.info("Player "+i+" (" + players[i].toString() + "): "+score[i]);
        }
	}
	
	/**
	 * Run method for a general referee.
	 * At the beginning of the game a InitGameMessage is sent, at the end a GameTerminateMessage is sent. To customize those two messages, overwrite the corresponding methods.
	 */
	@Override
	public void run() {
		Log.info("===================");
		Log.info("Starting "+this.getClass().getSimpleName());
		Log.info("===================");
		
		beforeGameStarts();
		
		// Send init game message to all players before game logic starts. 
		for(int i = 0; i < players.length; i++){
			sendInitGameMessage(createInitGameMessage(players[i]), i);
		}
		
		gameLoop();
		
		// Send game terminated message to all players before game logic ends. 
		for(int i = 0; i < players.length; i++){
			sendGameTerminatedMessage(createTerminateGameMessage(players[i]), i);
		}
		
		afterGameTerminated();
				
		Log.info("========================");
		Log.info("Shutting down "+this.getClass().getSimpleName());
		Log.info("========================");
	}

	/**
	 * Can be overriden by implementing referees
	 */
	protected void gameLoop() {
		while(!terminated && !logic.isTerminal(currentState)){
			beforeRound();
			
			Set<Integer> playersToAct = logic.getCurrentPlayersToAct(currentState);
	        Map<Integer, PlayActionMessage> messages;
			if (playersToAct.size()==1) {
				int playerToAct = playersToAct.iterator().next();
				messages = new HashMap<Integer, PlayActionMessage>();
				messages.put(playerToAct, players[playerToAct].genAction(createGenActionMessage(players[playerToAct])));
			} else {
				messages = requestActionsParallel(playersToAct);
			}
			
			List<Integer> actionOrder = resolveActionOrder(messages);
	        for (int playerId: actionOrder) {
	            Log.trace("Applying action of player " + playerId);
	            PlayActionMessage data = messages.get(playerId);
	            Action generatedAction = (data == null ? null : data.getAction());
	            if (generatedAction == null) {
	            	reactToNullAction(playerId, data);
	            } else {
	                Log.trace("Player: " + playerId + "(" + players[playerId].toString() + ") sent action: " + generatedAction.toString());
	                if (logic.validMove(currentState, generatedAction) && checkActionAllowedForPlayer(currentState, generatedAction, playerId)) {
	                	updateServerState(data);
	                	distributeAcceptedAction(data);
	                	reactToValidAction(playerId, data);
	                } else {
	                	reactToInvalidAction(playerId, data);
	                }
	            }
	        }
	        
	        afterRound(messages);
		}
	}
	
	@Override
	public void engineDisconnected(int id) {
		Log.error("Player " +  id + " (" + players[id].toString() + ") disconnected. Shutting down.");
		this.terminated = true;
	}
	
	protected boolean checkActionAllowedForPlayer(S fromState, Action action, int playerId) {
		return true;
	}

	protected void beforeRound() {
	}

	protected void afterRound(Map<Integer, PlayActionMessage> messages) {
		Log.info("Game state is now:");
		currentState.printPretty();
	}

	protected List<Integer> resolveActionOrder(Map<Integer, PlayActionMessage> messages) {
		List<Integer> order = new ArrayList<>(messages.keySet());
        Collections.shuffle(order);
        return order;
	}

	/**
	 * Distributes this action to all players
	 */
	protected void distributeAcceptedAction(PlayActionMessage action) {
		for (int i = 0; i < players.length; i++) {
			players[i].playMove(new PlayedMoveMessage(sensor.fromPointOfView(action.getAction(), currentState, i)));
		}
	}

	protected void updateServerState(PlayActionMessage m) {
		logic.makeMove(currentState, sensor.fromPointOfView(m.getAction(), currentState, -1)); //update server gamestate
	}

	protected void reactToInvalidAction(int player, PlayActionMessage m) {
		Log.info("Player " +  player + " (" + players[player].toString() + ") tried to make invalid action " + m.getAction());
	}
	
	protected void reactToNullAction(int player, PlayActionMessage m){
		Log.info("Player " +  player + " (" + players[player].toString() + ") didn't send a move in.");
	}
	
	protected void reactToValidAction(int player, PlayActionMessage m){
		
	}

    /**
     * Creates a new parallelMoveRequester that returns a hashmap of players to move. 
     * <b>If a player returned null, it is not contained in the hashmap.</b> (This is because a concurrent hash map cannot handle null values.)
     *
     * @return Map from players to their selected messages.
     */
    protected Map<Integer, PlayActionMessage> requestActionsParallel(Set<Integer> playerIDs){
    	return new ParallelActionRequester().requestActionsInParallel(playerIDs);
    }

    /**
     * Class for retrieving actions from each player in parallel.
     */
    private class ParallelActionRequester{
    	
        /** When requesting actions in parallel, the time between successive checks to see if all players have submitted messages. */
        private static final int CHECK_MOVE_INTERVAL = 1000;
    	
    	private int finishedThreads = 0;
    	
    	/**
    	 * Starts one thread for each player. The thread will ask the player to make an action and put it into the hashmap.
    	 * If the player returns null, it will not be added to the map.
    	 * @param playerIDs IDs of players to request actions for
    	 * @return Map from players to their selected messages.
    	 */
    	private Map<Integer, PlayActionMessage> requestActionsInParallel(Set<Integer> playerIDs){
    		Log.debug("GeneralReferee", "Requesting actions from players " + playerIDs);
    		// Data structure for moves of each player - needs to be thread-safe
            Map<Integer, PlayActionMessage> messages = new ConcurrentHashMap<>();
            // Generate each (alive) player's move
            for (int playerID : playerIDs) {
            	requestActionAsync(playerID, messages);
            }
            while(finishedThreads != playerIDs.size()){
	            synchronized (this) {
	            	try{
                        // TODO: Figure out this use of a wait interval - see issue 131
	            		this.wait(CHECK_MOVE_INTERVAL);
	            	}catch(InterruptedException e){
	            		// No logic necessary here.
	            	}
				}
	            Log.debug("Finished threads: "+finishedThreads);
            }
            return messages;
    	}
    	
    	/**
    	 * Requests one action from the specified player. Increases count of finished threads after message is received.
    	 */
    	private void requestActionAsync(final int playerId, final Map<Integer, PlayActionMessage> map){
    		new Thread(){
    			@Override
				public void run() {
    				Log.trace("Starting thread for player "+playerId);
                    Log.trace("Asking player " + playerId + " for action");
                    PlayActionMessage message = players[playerId].genAction(createGenActionMessage(players[playerId]));
                    if (message != null){
                    	map.put(playerId, message);
                    } else {
                        Log.trace("Player: " + playerId + " sent in null move");
                    }
    				
    				synchronized (ParallelActionRequester.this) {
    					finishedThreads++;
    					ParallelActionRequester.this.notifyAll();
					}
    				Log.trace("Finished thread for player "+playerId);
    			};
    		}.start();
		}
    }
}
