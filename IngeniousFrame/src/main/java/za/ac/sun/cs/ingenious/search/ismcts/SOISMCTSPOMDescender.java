package za.ac.sun.cs.ingenious.search.ismcts;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SearchValue;

/**
 * Implementation of TreeDescender for SOISMCTS-POM. Differences to SOISMCTS: moves leading out from a
 * node correspond to moves as seen by the player the search TreeEngine belongs to. When the TreeEngine is being
 * descended, the determinization is updated with a random action compatible with the observed move.
 */
public class SOISMCTSPOMDescender<S extends TurnBasedGameState> extends SOISMCTSDescender<S> {

	public SOISMCTSPOMDescender(GameLogic<S> logic, SearchValue<S, ISMCTSNode<S>> searchValue,
			ActionSensor<S> sensor, int treeObserverID, InformationSetDeterminizer<S> det) {
		super(logic, searchValue, sensor, treeObserverID, det);
	}
	
	@Override
	protected void updatingDeterminizedState(ISMCTSNode<S> parent, Move toExpand) {
		if (toExpand instanceof Action) {
			super.updatingDeterminizedState(parent, toExpand);
		} else {
			// Choose an action compatible with the move to expand.
			List<Action> allActions = logic.generateActions(parent.getGameState(), parent.getCurrentPlayer());
			List<Action> val = new ArrayList<Action>();
			for (Action a : allActions) {
				if (sensor.fromPointOfView(a, this.determinizedState, treeObserverID).equals(toExpand)) {
					val.add(a);
				}
			}
			
			Action randomAction = val.get((int) (Math.random() * val.size()));
			logic.makeMove(this.determinizedState, sensor.fromPointOfView(randomAction, this.determinizedState, -1));
		}
	}
	
	@Override
	protected ISMCTSNode<S> createNodeForMove(Move move, ISMCTSNode<S> parent) {		
		@SuppressWarnings("unchecked")
		S newState = (S) parent.getGameState().deepCopy();
		if (move instanceof Action) {
			this.logic.makeMove(newState, sensor.fromPointOfView((Action) move, newState, treeObserverID));
		} else {
			this.logic.makeMove(newState, move);			
		}
		List<Action> allActions = logic.generateActions(newState, newState.nextMovePlayerID);
		if (newState.nextMovePlayerID == treeObserverID) {
			return new ISMCTSNode<S>(newState, logic, move, parent, new ArrayList<Move>(allActions));
		} else {
			// Initialize the list of unexpanded moves to the moves observed by the player whom this
			// search TreeEngine belongs to.
			List<Move> val = new ArrayList<Move>();
			for (Action a : allActions) {
				Move m = sensor.fromPointOfView(a, newState, treeObserverID);
				if (!val.contains(m)) {
					val.add(m);
				}
			}
			return new ISMCTSNode<S>(newState, logic, move, parent, val);			
		}
	}

}
