package za.ac.sun.cs.ingenious.core.util.sensor;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.Move;

/**
 * This is a simple implementation of {@link Action Sensor} that can be used for all perfect
 * information games. Here, the move observed by any player for some action is always the action
 * itself.
 * 
 * @param <S> GameState of the game that this observer can be used for.
 */
public class PerfectInformationActionSensor<S extends GameState> implements ActionSensor<S> {

	@Override
	public Move fromPointOfView(Action originalAction, S fromGameState, int pointOfViewPlayerID) {
		return originalAction;
	}

}
