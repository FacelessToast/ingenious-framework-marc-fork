package za.ac.sun.cs.ingenious.games.mnk;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.util.state.TurnBased2DBoard;

public class MNKFinalEvaluator implements GameFinalEvaluator<MNKState> {

	public int getWinner(MNKState state) {
		for (int i = 0; i < state.playerBoards.size(); i++) {
			TurnBased2DBoard board = state.playerBoards.get(i);
			// Rows
			for (int row = 0; row < board.getHeight(); row++) {
				int last = 0;
				int in_A_Row = 0;
				for (int col = 0; col < board.getWidth(); col++) {
					int curr = board.board[row][col];

					if (curr != 0 && curr == last) {
						in_A_Row++;
						if (in_A_Row == state.getK() - 1) {
							return curr - 1;
						}
					} else {
						in_A_Row = 0;
					}
					last = curr;
				}
			}

			// Collums
			for (int col = 0; col < board.getWidth(); col++) {
				int last = 0;
				int in_A_Row = 0;
				for (int row = 0; row < board.getHeight(); row++) {
					int curr = board.board[row][col];

					if (curr != 0 && curr == last) {
						in_A_Row++;
						if (in_A_Row == state.getK() - 1) {
							return curr - 1;
						}
					} else {
						in_A_Row = 0;
					}
					last = curr;
				}
			}

			// Diagonals (right)
			// For each field
			for (int row = 0; row < board.getHeight(); row++) {
				for (int col = 0; col < board.getWidth(); col++) {
					// Try to descend
					int last = 0;
					int in_A_Row = 0;
					for (int r = row, c = col; r < board.getHeight() && c < board.getWidth(); r++, c++) {
						int curr = board.board[r][c];
						if (curr != 0 && curr == last) {
							in_A_Row++;
							if (in_A_Row == state.getK() - 1) {
								return curr - 1;
							}
						} else {
							in_A_Row = 0;
						}
						last = curr;
					}
				}
			}

			// Diagonals (left)
			// For each field
			for (int row = 0; row < board.getHeight(); row++) {
				for (int col = 0; col < board.getWidth(); col++) {
					// Try to descend
					int last = 0;
					int in_A_Row = 0;
					for (int r = row, c = col; r < board.getHeight() && r >= 0 && c < board.getWidth()
							&& c >= 0; r++, c--) {
						int curr = board.board[r][c];
						if (curr != 0 && curr == last) {
							in_A_Row++;
							if (in_A_Row == state.getK() - 1) {
								return curr - 1;
							}
						} else {
							in_A_Row = 0;
						}
						last = curr;
					}
				}
			}

			if (full(board)) {
				return -1; // draw
			}
		}
		return -2; // not yet terminal
	}

	private boolean full(TurnBased2DBoard state) {
		for (int i = 0; i < state.getHeight(); i++) {
			for (int j = 0; j < state.getWidth(); j++) {
				if (state.board[i][j] == 0) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public double[] getScore(MNKState forState) {
		double[] scores = new double[2];
		int winner = getWinner(forState); 
		if(winner == -1 || winner == -2){
			scores[0] = 0.5;
			scores[1] = 0.5;
		}else{
			scores[winner] = 1;
		}
		return scores;
	}
}
