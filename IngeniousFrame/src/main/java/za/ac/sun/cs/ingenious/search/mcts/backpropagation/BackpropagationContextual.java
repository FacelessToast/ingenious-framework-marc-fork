package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.CMCTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.MctsCMCNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

import java.util.Hashtable;

public class BackpropagationContextual<N extends MctsNodeTreeParallelInterface<?, ?, ?>> implements BackpropagationThreadSafe<N> {

    public CMCTable visitedMovesTable = new CMCTable();

    public BackpropagationContextual() {
    }

    /**
     * Update the fields relating to this backpropagation strategy for the current node.
     * @param node
     * @param results the win/loss or draw outcome for the simulation relating to the current playout.
     */
    public void propagate(N node, double[] results) {
        N parent = (N)node.getParent();
        double playerResults = results[parent.getPlayerID()];
        boolean win = true;
        for (double i : results) {
            if (i > playerResults) {
                win = false;
                break;
            }
        }

        Hashtable<String, MctsNodeExtensionParallelInterface> enhancementsNode = node.getEnhancementClasses();

        // get extension object
        node.readLockEnhancementClassesArrayList();
        MctsCMCNodeExtensionParallel contextualEnhancementExtensionNode = (MctsCMCNodeExtensionParallel)enhancementsNode.get("CMC");
        node.readUnlockEnhancementClassesArrayList();

        if (contextualEnhancementExtensionNode == null) {
            System.exit(1);
            System.out.println("No CMC enhancement to backprop from in CMC backpropagation class");
        }

        N grandparent = (N)parent.getParent();
        if (grandparent != null && grandparent.getPrevAction() != null) {
            // Node is at least 3 levels deep in the TreeEngine
            Action firstTileAction = grandparent.getPrevAction();
            Action secondTileAction = node.getPrevAction();

            CMCTable.StoredActions storedAction =  visitedMovesTable.getMoveCombinationScores().get(firstTileAction);

            if (storedAction == null) {
                // create new stored action and update memory table
                CMCTable.StoredActions newStoredAction = new CMCTable().createNewStoredAction(secondTileAction, !win);
                visitedMovesTable.getMoveCombinationScores().put(firstTileAction, newStoredAction);
            } else {
                storedAction.setReadLock();
                if (storedAction.actions.contains(secondTileAction)) {
                    // update the memory table where tile 1 and 2 are already in the memory table
                    storedAction.unsetReadLock();
                    storedAction.setWriteLock();
                    storedAction.incVisitCount(secondTileAction);
                    if (win) {
                        storedAction.incWins(secondTileAction);
                    }
                    storedAction.unsetWriteLock();
                } else {
                    // update the memory table where tile 1 is already in the memory table but not tile 2
                    storedAction.unsetReadLock();
                    storedAction.setWriteLock();
                    storedAction.addNewAction(secondTileAction);
                    storedAction.incVisitCount(secondTileAction);
                    if (win) {
                        storedAction.incWins(secondTileAction);
                    }
                    storedAction.unsetWriteLock();
                }
            }
        }
    }

    /**
     * @return the global table containing the move information relating to CMC.
     */
    public CMCTable getCMCTable() {
        return visitedMovesTable;
    }

    public static <NN extends MctsNodeTreeParallelInterface<?, ?, ?>> BackpropagationContextual<NN> newBackpropagationContextual() {
        return new BackpropagationContextual<>();
    }
}
