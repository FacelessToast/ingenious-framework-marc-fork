#!/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client
#java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client \
#-username "bob" -config "EnhancementChoices/EnhancementChoiceVanilla.json"\
#-engine "za.ac.sun.cs.ingenious.games.othello.engines.OthelloRandomEngine" \
#-game "OthelloReferee" -hostname localhost -port 61234 -threadCount 4 &
#
#java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client \
#-username "alice" -config "EnhancementChoices/EnhancementVanilla.json"\
#-engine "za.ac.sun.cs.ingenious.games.othello.engines.OthelloTreeMctsEngine" \
#-game "OthelloReferee" -hostname 127.0.0.1 -port 61234 -threadCount 4


for i in `seq 1 $1`
do
  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "Othello.json" -game "OthelloReferee" -lobby "mylobby"
  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "$2_player1" -config "EnhancementChoices/EnhancementChoice$2.json" -lobby "mylobby" -engine "za.ac.sun.cs.ingenious.games.othello.engines.OthelloTreeMctsEngine" -threadCount 4 -game "OthelloReferee" -hostname localhost -port 61234 &
  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "$3_player2" -config "EnhancementChoices/EnhancementChoice$3.json" -lobby "mylobby" -engine "za.ac.sun.cs.ingenious.games.othello.engines.OthelloTreeMctsEngine" -threadCount 4 -game "OthelloReferee" -hostname 127.0.0.1 -port 61234
  echo "-" -n
done