BEGIN {
    p1_count = 0
    p2_count = 0
    score_count = 0
}
{
	if (($0 ~ (p1 ":"))) {
		split($0, a, ":")
		for (i in a) {
			if (a[i] ~ p1) {
				print p1 ": " a[i+1]
				p1_count = p1_count + a[i+1]
			}	
		}


		
	} else if ($0 ~ (p2 ":")) {
		split($0, a, ":")
		for (i in a) {
			if (a[i] ~ p2) {
				print p2 ": " a[i+1]
				p2_count = p2_count + a[i+1]
			}	
		}
	}
	
}
END {
	print p1 " : " p1_count " - " (p1_count/(p1_count + p2_count))*100 "%"
    	print p2 " : " p2_count " - " (p2_count/(p1_count + p2_count))*100 "%"
}
